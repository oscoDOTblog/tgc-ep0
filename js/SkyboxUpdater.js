import {Component, Property} from '@wonderlandengine/api';

function updateSkybox() {
    // Get references to the Skybox Renderer and Skybox Material
    const skyboxRenderer = this.getComponent("SkyboxRenderer");
    const skyboxMaterial = this.getScene().getMaterial("SkyboxMaterial");
  
    // Generate a new skybox image or modify the existing material parameters here
    // Example: skyboxMaterial.setParameter("albedo", newColor);
    skyboxMaterial.setParameter("albedo", newColor);

    // Assign the updated skybox material to the renderer
    skyboxRenderer.setMaterial(skyboxMaterial);
}

/**
 * SkyboxUpdater
 */
export class SkyboxUpdater extends Component {
    static TypeName = 'SkyboxUpdater';
    /* Properties that are configurable in the editor */
    static Properties = {
        param: Property.float(1.0),
        sMat: Property.material(),
        sText: Property.texture()
    };

    static onRegister(engine) {
        /* Triggered when this component class is registered.
         * You can for instance register extra component types here
         * that your component may create. */
    }

    init() {
        console.log('init() with param', this.param);
        console.log(this.sMat)
        // this.sMat.setMaterial()
        this.sMat.texture = this.sText // i cant believe this shit works LMFAOOOO
        // const material = mesh.material
    }

    start() {
        console.log('start() with param', this.param);
    }

    update(dt) {
        /* Called every frame. */
    }
}
